<?php
    $html_content = get_field('brief_email_template','option');
    while ( have_posts() ) : the_post();
        $title = get_the_title();
        $content = get_the_content();
        $categories = get_the_category();
    endwhile;
    $html_content = str_replace("{{title}}", $title, $html_content);
    $html_content = str_replace("{{content}}", $content, $html_content);
    echo $html_content;
    die();
?>