<?php
/**
 * The default header for the theme.
 *
 * @package Morning Consult 2015
 */
get_template_part('inc/partials/header','opening'); ?>

<?php $section_cat = get_field('section_home_category'); ?>

<header id="header-main" class="site-header <?php echo $section_cat ? $section_cat->slug : ""; ?>" role="banner">
    <nav class="navbar">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    <img class="logo hidden-xs" src="<?php bloginfo('template_url'); ?>/img/logo.png">
                    <img class="logo visible-xs-inline" src="<?php bloginfo('template_url'); ?>/img/logo_square.svg">
                </a>
            </div>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".nav-main">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php if (is_page('subscribe') === false) : ?>
                <div class="collapse navbar-collapse nav-main navbar-right subscribe-link">
                    <a href="#" class="btn btn-default btn-inverted navbar-btn" data-toggle="modal" data-target=".subscribe-modal">Subscribe</a>
                </div>
            <?php endif; ?>
        </div>
    </nav>
    <nav class="navbar lower hasColor">
        <div class="container">

            <?php
                wp_nav_menu( array(
                    'menu'              => 'primary',
                    'theme_location'    => 'primary',
                    'depth'             => 2,
                    'container'         => 'div',
                    'container_class'   => 'collapse navbar-collapse navbar-left nav-main',
                    'container_id'      => 'nav-main',
                    'menu_class'        => 'nav navbar-nav',
                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                    'walker'            => new wp_bootstrap_navwalker())
                );
            ?>

            <?php
                wp_nav_menu( array(
                    'menu'              => 'secondary',
                    'theme_location'    => 'secondary',
                    'depth'             => 2,
                    'container'         => 'div',
                    'container_class'   => 'collapse navbar-collapse navbar-right',
                    'container_id'      => 'nav-secondary',
                    'menu_class'        => 'nav navbar-nav',
                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                    'walker'            => new wp_bootstrap_navwalker())
                );
            ?>
        </div>
    </nav>
</header>