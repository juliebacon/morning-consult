<?php
/**
 * The template for displaying the footer.
 *
 * @package Morning Consult 2015
 */
?>

<footer id="footer-main" role="contentinfo">
    <?php if(is_active_sidebar('footer-main')) : ?>
        <div class="container">
            <?php dynamic_sidebar( 'footer-main' ); ?>
        </div>
    <?php endif; ?>
</footer><!-- #footer-main -->

<?php wp_reset_query(); ?>
<?php if (is_page('subscribe') === false) : ?>
<div class="modal fade subscribe-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php the_field('subscribe_modal_content','option'); ?>
    </div>
  </div>
</div>
<?php endif; ?>

<?php wp_footer(); ?>

</body>
</html>
