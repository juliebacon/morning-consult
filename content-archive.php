<?php
/**
 * The template part for displaying posts in archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Morning Consult 2015
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
    </header><!-- .entry-header -->

    <div class="entry-summary">
        <?php the_excerpt(); ?>
    </div><!-- .entry-summary -->

    <footer class="entry-footer meta">
        <?php morning_consult_category_links(); ?> |
        <a class="timeago" href="<?php echo get_permalink(); ?>">
            <?php the_time( 'F j' ); ?>
        </a>
    </footer>
</article><!-- #post-## -->
