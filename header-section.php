<?php
/**
 * The default header for the theme.
 *
 * @package Morning Consult 2015
 */
?>

<?php $section_cat = get_field('section_home_category'); ?>

<?php get_template_part('inc/partials/header','opening'); ?>

<header id="header-main" class="site-header section-header <?php echo $section_cat->slug; ?>" role="banner">
    <nav class="navbar">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    <img class="logo" src="<?php bloginfo('template_url'); ?>/img/logo.svg">
                </a>
            </div>
        </div>
    </nav>
    <nav class="navbar lower">
        <div class="container">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-main">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <?php
                wp_nav_menu( array(
                    'menu'              => 'section-home',
                    'theme_location'    => 'section-home',
                    'depth'             => 2,
                    'container'         => 'div',
                    'container_class'   => 'collapse navbar-collapse navbar-left',
                    'container_id'      => 'nav-main',
                    'menu_class'        => 'nav navbar-nav',
                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                    'walker'            => new wp_bootstrap_navwalker())
                );
            ?>

            <?php
                wp_nav_menu( array(
                    'menu'              => 'section-home-right',
                    'theme_location'    => 'section-home-right',
                    'depth'             => 2,
                    'container'         => 'div',
                    'container_class'   => 'collapse navbar-collapse navbar-right',
                    'container_id'      => 'nav-secondary',
                    'menu_class'        => 'nav navbar-nav',
                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                    'walker'            => new wp_bootstrap_navwalker())
                );
            ?>
        </div>
    </nav>
</header>