jQuery(document).ready(function($){
    $('#post-body').on('click', '.morning-brief-create-campaign', function(event) {
        event.preventDefault();
        var self = this;
        var response_container = $(this).parents('.acf-input').find('.response');

        $.post(
            ajaxurl,
            {
                "action": 'mc_create_campaign',
                "url": window.location.href,
                "emailsystem": $(self).data('emailsystem')
            },
            function(response) {
                if (response.success === true) {
                    response_container.html("Success! The campaign ID is " + response.data + ".");
                } else {
                    response_container.html("There was a problem creating the campaign. Here is the error message:<br> " + response.data);
                }
            }
        );
    });
});