jQuery(function($) {
    // Morning Consult Variables
    var lastScrollTop = 0;
    var buffer = 20;
    var navbarHeight = $('#header-main').outerHeight();
    var backstretch_options = {};

    if (window.featured_story_image) {
        // TODO: load based on screen size
        if ($('body').hasClass('author')) {
            backstretch_options.centeredY = false;
        }
        $('.featured').backstretch(window.featured_story_image.url, backstretch_options);
    }

    $('.gform_wrapper').on('submit', '[target^="gform_ajax"]', function(event) {
        var spinner = $(this).find('.gform_ajax_spinner');
        spinner.velocity(
            {rotateZ: "360deg"},
            {
                duration: 2000,
                easing: "linear",
                loop: true
            }
        );
    });

    $(window).on('resize', function(event) {
        resize_charts();
    });

    function resize_charts() {
        $('[id^=visualizer-]').each(function(index, el) {
            resize_chart(el);
        });
    }

    function resize_chart(chart, new_height) {
        var id = $(chart).attr('id');
        var curwidth = $(chart).width();
        var newheight = curwidth / (4/3);
        if (visualizer && visualizer.charts && visualizer.charts[id] && google && google.visualization) {
            visualizer.charts[id].settings['height'] = newheight;
        }
    }

    resize_charts();

    $('[data-bg]').each(function(index, el) {
        $(this).backstretch($(this).attr('data-bg'));
    });

});