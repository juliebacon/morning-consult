<?php
/**
 * The template for displaying all single posts.
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php if ( has_post_thumbnail() ) : ?>
			<div class="featured">
			</div>
			<?php
			    $post_featured_image = morning_consult_get_featured_image_info();
			    wp_localize_script( 'morning-consult-all-js', 'featured_story_image', $post_featured_image );
			?>

			<?php if ($post_featured_image['caption']) : ?>
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<p class="featured-image-caption"><?php echo $post_featured_image['caption']; ?></p>
						</div>
					</div>
				</div>
			<?php endif; ?>
		<?php endif; ?>

		<div class="single">

			<?php get_template_part( 'content', 'single' ); ?>

			<?php locate_template('inc/partials/single/related.php', true, false); ?>

		</div>

	<?php endwhile; ?>

<?php get_footer(); ?>
