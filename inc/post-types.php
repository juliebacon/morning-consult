<?php

// Register Custom Post Type
function morning_consult_post_types() {

    /**
     * Brief
     */
    $brief_labels = array(
        'name'                => 'Briefs',
        'singular_name'       => 'Brief',
        'menu_name'           => 'Briefs',
        'parent_item_colon'   => 'Parent Brief:',
        'all_items'           => 'All Briefs',
        'view_item'           => 'View Brief',
        'add_new_item'        => 'Add New Brief',
        'add_new'             => 'Add New',
        'edit_item'           => 'Edit Brief',
        'update_item'         => 'Update Brief',
        'search_items'        => 'Search Briefs',
        'not_found'           => 'Not found',
        'not_found_in_trash'  => 'Not found in Trash',
    );

    $brief_args = array(
        'labels'              => $brief_labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'custom-fields', ),
        'taxonomies'          => array( 'category', 'post_tag' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => 'briefs',
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'rewrite'             => array ('slug' => 'briefs')
    );

    register_post_type( 'mc_brief', $brief_args );

    /**
     * Polling
     */

    $poll_labels = array(
        'name'                => 'Polls',
        'singular_name'       => 'Poll',
        'menu_name'           => 'Polls',
        'parent_item_colon'   => 'Parent Poll:',
        'all_items'           => 'All Polls',
        'view_item'           => 'View Poll',
        'add_new_item'        => 'Add New Poll',
        'add_new'             => 'Add New',
        'edit_item'           => 'Edit Poll',
        'update_item'         => 'Update Poll',
        'search_items'        => 'Search Polls',
        'not_found'           => 'Not found',
        'not_found_in_trash'  => 'Not found in Trash',
    );

    $poll_args = array(
        'labels'              => $poll_labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'custom-fields', ),
        'taxonomies'          => array( 'category', 'post_tag' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => 'polls',
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'rewrite'             => array ('slug' => 'polls')
    );

    register_post_type( 'mc_poll', $poll_args );

    /**
     * Opinion
     */

    $opinion_labels = array(
        'name'                => 'Opinions',
        'singular_name'       => 'Opinion',
        'menu_name'           => 'Opinions',
        'parent_item_colon'   => 'Parent Opinion:',
        'all_items'           => 'All Opinions',
        'view_item'           => 'View Opinion',
        'add_new_item'        => 'Add New Opinion',
        'add_new'             => 'Add New',
        'edit_item'           => 'Edit Opinion',
        'update_item'         => 'Update Opinion',
        'search_items'        => 'Search Opinions',
        'not_found'           => 'Not found',
        'not_found_in_trash'  => 'Not found in Trash',
    );

    $opinion_args = array(
        'labels'              => $opinion_labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'custom-fields', ),
        'taxonomies'          => array( 'category', 'post_tag' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => 'opinions',
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'rewrite'             => array ('slug' => 'opinions')
    );

    register_post_type( 'mc_opinion', $opinion_args );

}

// Hook into the 'init' action
add_action( 'init', 'morning_consult_post_types', 0 );