<div class="container quote">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <blockquote>
                <?php the_sub_field('quote_content'); ?>
                <?php if (get_sub_field('quote_source')) : ?>
                    <span class="source">- <?php the_sub_field('quote_source'); ?></span>
                <?php endif; ?>
            </blockquote>
        </div>
    </div>
</div>