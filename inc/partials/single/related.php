<?php
    // Related posts

    $current_post_cat = morning_consult_which_category();
    $current_post_type = $post->post_type;

    $related_posts_args = array (
        'post_type'              => $current_post_type,
        'posts_per_page'         => 4,
        'category_name'          => $current_post_cat,
        'post__not_in'           => array($post->ID)
    );

    $related_posts = new WP_Query($related_posts_args);

    if( $related_posts->have_posts() && $current_post_type !== "mc_brief" ):
    ?>

        <div class="related">
            <div class="container">
                <h3>More <?php echo get_category_by_slug($current_post_cat)->name;?> <?php echo get_post_type_object($post->post_type)->labels->name; ?></h3>

                <div class="row">
                    <div class="col-xs-6">
                        <?php while( $related_posts->have_posts() ): $related_posts->the_post(); ?>

                            <?php if ($related_posts->current_post === 2 ) : ?>
                            </div>
                            <div class="col-xs-6">
                            <?php endif; ?>

                            <article id="post-<?php the_ID(); ?>" <?php post_class('excerpt'); ?>>

                                <header class="entry-header">
                                    <?php the_title( '<h4 class="entry-title"><a href="' . get_permalink() . '">', '</a></h4>' ); ?>
                                </header>

                                <div class="entry-content">
                                    <?php the_excerpt(); ?>
                                </div>
                            </article>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; wp_reset_postdata(); ?>