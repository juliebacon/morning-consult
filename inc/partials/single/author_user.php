        <?php
            $author_headshot = get_field('author_headshot', 'user_' . $author->ID);
        ?>

        <div class="author">
            <div class="container">
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        <?php if ($author_headshot) : ?>
                            <div class="author-photo">
                                <img src="<?php echo $author_headshot['sizes']['thumbnail']; ?>" alt="Author Photo">
                            </div>
                        <?php endif; ?>
                        <div class="author-info">
                            <h4 class="name"><?php echo get_the_author_meta( 'first_name', $author->ID ); ?> <?php echo get_the_author_meta( 'last_name', $author->ID ); ?></h4>
                            <?php
                                $author_twitter = strtolower(get_the_author_meta( 'twitter', $author->ID ));
                                if ($author_twitter && strpos($author_twitter, 'twitter.com') !== false) :
                                    echo '<a class="twitter" target="_blank" href="' . $author_twitter . '">@' . substr($author_twitter, strlen('twitter.com/') + strpos($author_twitter, 'twitter.com/')) . '</a>';
                                elseif ($author_twitter) :
                                    echo '<a class="twitter" target="_blank" href="http://www.twitter.com/' . $author_twitter . '">@' . $author_twitter . '</a>';
                                endif;
                            ?>
                            <p><?php echo get_the_author_meta('description', $author->ID); ?></p>
                            <a class="author-link" href="<?php echo get_author_posts_url($author->ID); ?>">See all posts by <?php echo get_the_author_meta( 'first_name', $author->ID ); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>