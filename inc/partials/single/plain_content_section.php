<div class="container plain-content">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
            <?php the_sub_field('content'); ?>
        </div>
    </div>
</div>