<?php
    $wide_ad_1 = get_sub_field('ad_source');

    if ($wide_ad_1 && $wide_ad_1 === "Adsense") : ?>

        <div class="wide-ad-area adsense page-specific">
            <div class="ad-wrap">
                <?php the_sub_field('adsense_code'); ?>
            </div>
        </div>

    <?php elseif ($wide_ad_1 && $wide_ad_1 === "Custom (sidebar)") : ?>

        <?php if(is_active_sidebar('wide-ad-area')) : ?>
            <div class="wide-ad-area sidebar page-specific">
                <div class="ad-wrap">
                    <?php dynamic_sidebar( 'wide-ad-area' ); ?>
                </div>
            </div>
        <?php endif; ?>

    <?php elseif ($wide_ad_1 && $wide_ad_1 === "Custom (upload)") : ?>

        <div class="wide-ad-area custom-link">
            <div class="ad-wrap">
                <a target="_blank" href="<?php the_sub_field('ad_link'); ?>">
                    <img src="<?php the_sub_field('ad_image'); ?>" alt="advertisement">
                </a>
            </div>
        </div>

    <?php elseif (!$wide_ad_1 || $wide_ad_1 === "Site Default") : ?>

        <?php $wide_ad_1_site = get_field('wide_ad_unit_1_type','option'); ?>

        <?php if ($wide_ad_1_site && $wide_ad_1_site === "Adsense") : ?>

            <div class="wide-ad-area adsense site-default">
                <div class="ad-wrap">
                    <?php the_field('wide_ad_unit_1_code','option'); ?>
                </div>
            </div>

        <?php elseif ($wide_ad_1_site && $wide_ad_1_site === "Custom (Sidebar)") : ?>

            <?php if(is_active_sidebar('wide-ad-area')) : ?>
                <div class="wide-ad-area sidebar site-default">
                    <div class="ad-wrap">
                        <?php dynamic_sidebar( 'wide-ad-area' ); ?>
                    </div>
                </div>
            <?php endif; ?>

        <?php endif; ?>

    <?php endif; ?>