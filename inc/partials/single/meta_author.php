<?php if ($authorkey < (count($all_authors) - 1)) {
    $comma_after_author = " and ";
} else {
    $comma_after_author = "";
} ?>

<?php if (get_class($author) === "WP_User") : ?>
    <a class="url fn n" href="<?php echo esc_url( get_author_posts_url( $author->ID ) ); ?>"><?php echo get_the_author_meta( 'first_name', $author->ID ); ?> <?php echo get_the_author_meta( 'last_name', $author->ID ); ?></a>
    <?php echo $comma_after_author; ?>
<?php elseif (get_class($author) === "stdClass") : ?>
    <a class="url fn n" href="/author/<?php echo $author->user_nicename; ?>"><?php echo $author->first_name; ?> <?php echo $author->last_name; ?><?php echo $comma_after_author; ?></a>
<?php endif; ?>