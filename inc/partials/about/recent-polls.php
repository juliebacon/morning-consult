<?php
    $recent_polls_args = array (
        'post_type'              => 'mc_poll',
        'posts_per_page'         => '4',
    );

    $recent_polls = new WP_Query($recent_polls_args);

    if( $recent_polls->have_posts() ): while( $recent_polls->have_posts() ): $recent_polls->the_post();
    ?>

    <div class="col-xs-12 col-sm-6 col-md-3">
        <div class="thumbnail-wrap">
            <a href="<?php the_permalink(); ?>">
                <?php if ( has_post_thumbnail() ) {
                    the_post_thumbnail('medium');
                } ?>
                <h3 class="title"><?php the_title(); ?></h3>
            </a>
        </div>
    </div>

    <?php
    endwhile;
    endif;
    wp_reset_postdata();
?>