<?php

if( have_rows('about_who_people') ):
    $about_people_counter = 0;
    while ( have_rows('about_who_people') ) : the_row();

    if ($about_people_counter % 4 === 0) {
        echo '</div><div class="row">';
    }

    ?>
        <div class="col-xs-6 col-sm-3">
            <?php
                $author = get_sub_field('user');
                $author_headshot = get_field('author_headshot', 'user_' . $author['ID']);
                echo '<div class="author"><a href="' . get_author_posts_url($author['ID']) . '">';
                echo '<img src="' . $author_headshot['sizes']['medium'] . '" alt="' . $author['user_firstname'] . ' ' . $author['user_lastname'] . '">';
                echo "<span>" . $author['user_firstname'] . ' ' . $author['user_lastname'] . "</span>";
                echo '</a></div>';
            ?>
        </div>
    <?php

    $about_people_counter++;
    endwhile;
endif;