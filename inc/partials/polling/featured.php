<?php
/**
 * The featured polls in the polling center.
 *
 * @package Morning Consult 2015
 */
?>

<?php
    global $post;
    $polling_page = $post;

    $polling_used_categories = array();

    $featured_categories = get_field('polling_center_featured_categories');
?>

    <?php
        // Featured poll
        // Using this so that we don't lose variables in the current scope
        include(locate_template('inc/partials/polling/featured-poll.php'));
        $featured_categories = array_diff($featured_categories, $polling_used_categories);
    ?>

    <div class="sub-featured">
        <div class="container">
            <div class="row">
                <?php
                    // Sub featured polls
                    $polling_sub_featured_args = array (
                        'post_type'              => 'mc_poll',
                        'posts_per_page'         => '100',
                        'category__in'           => $featured_categories
                    );

                    $polling_sub_featured = new WP_Query($polling_sub_featured_args);

                    if( $polling_sub_featured->have_posts() ): while( $polling_sub_featured->have_posts() ): $polling_sub_featured->the_post();

                        $sub_featured_poll_category = get_the_category();

                        $exclude_from_recent_polls[] = $post->ID;

                        // This will make sure we're only showing one poll per category in the featured section.
                        if (in_array($sub_featured_poll_category[0]->term_id, $polling_used_categories)) {
                            continue;
                        } else {
                            $polling_used_categories[] = $sub_featured_poll_category[0]->term_id;
                        } ?>

                        <div class="col-sm-4">
                            <article id="post-<?php the_ID(); ?>" <?php post_class('excerpt'); ?>>

                                <?php
                                    if (get_field('poll_featured_chart')) :
                                        echo do_shortcode(get_field('poll_featured_chart'));
                                    elseif ( has_post_thumbnail() ) :
                                       the_post_thumbnail('large');
                                    endif;
                                ?>

                                <header class="entry-header">
                                    <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '">', '</a></h3>' ); ?>
                                </header>

                                <div class="entry-content">
                                    <?php the_excerpt(); ?>
                                </div>

                                <footer class="entry-footer meta">
                                    <?php morning_consult_category_links(); ?> |
                                    <a class="timeago" href="<?php echo get_permalink(); ?>">
                                        <?php the_time( 'F j' ); ?>
                                    </a>
                                </footer>
                            </article>
                        </div>

                        <?php if (count($polling_used_categories) > 3) {
                            break;
                        } ?>

                    <?php endwhile; endif;
                    wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>