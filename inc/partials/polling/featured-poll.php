<?php
/**
 * The featured story on the polling center.
 *
 * @package Morning Consult 2015
 */

    $polling_featured_args = array (
        'post_type'              => 'mc_poll',
        'posts_per_page'         => '1',
        'category__in'           => $featured_categories
    );

    $polling_featured = new WP_Query($polling_featured_args);

    if( $polling_featured->have_posts() ): while( $polling_featured->have_posts() ): $polling_featured->the_post();

    $featured_poll_category = get_the_category();

    $polling_used_categories[] = $featured_poll_category[0]->term_id;

    $exclude_from_recent_polls[] = $post->ID;

    ?>

        <div class="featured">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <header class="entry-header">
                            <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                            <a class="btn btn-default btn-inverted read-more" href="<?php echo esc_url( get_permalink() ); ?>">Read more</a>
                        </header><!-- .entry-header -->
                    </div>
                </div>
            </div>
        </div>

        <?php
            $section_home_featured_story_image = morning_consult_get_featured_image_info();
            wp_localize_script( 'morning-consult-all-js', 'featured_story_image', $section_home_featured_story_image );
        ?>

    <?php endwhile; endif; wp_reset_postdata(); ?>