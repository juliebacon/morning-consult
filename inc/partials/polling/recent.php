<?php
/**
 * The loop for posts in the 'recent polls' section of the polling center.
 *
 * @package Morning Consult 2015
 */
?>

<div class="recent">
    <div class="container">
        <h2 class="section-title">Recent Polls</h2>
        <div class="row">
            <div class="col-sm-4">

                <?php

                    $polling_recent_args = array (
                        'post_type'              => 'mc_poll',
                        'posts_per_page'         => '6',
                        'post__not_in'           => $exclude_from_recent_polls
                    );

                    $polling_recent = new WP_Query($polling_recent_args);

                    if( $polling_recent->have_posts() ): while( $polling_recent->have_posts() ): $polling_recent->the_post();

                    if ($polling_recent->current_post === 2) :
                        echo '</div>';
                        echo '<div class="col-sm-4 middle-column">';
                    elseif (($polling_recent->current_post % 2) === 0 && $polling_recent->current_post > 0) :
                        echo '</div>';
                        echo '<div class="col-sm-4">';
                    endif;

                ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class('excerpt'); ?>>

                        <?php
                            if (get_field('poll_featured_chart')) :
                                echo do_shortcode(get_field('poll_featured_chart'));
                            elseif ( has_post_thumbnail() ) :
                               the_post_thumbnail('large');
                            endif;
                        ?>

                        <header class="entry-header">
                            <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '">', '</a></h3>' ); ?>
                        </header>

                        <div class="entry-content">
                            <?php the_excerpt(); ?>
                        </div>

                        <footer class="entry-footer meta">
                            <?php morning_consult_category_links(); ?> |
                            <a class="timeago" href="<?php echo get_permalink(); ?>">
                                <?php the_time( 'F j' ); ?>
                            </a>
                        </footer>
                    </article>

                <?php
                    endwhile; endif;
                    wp_reset_postdata();
                ?>
            </div>
        </div><!-- row -->
        <div class="row">
            <div class="col-xs-12">
                <?php if (get_field('section_home_read_more_stories_link', $section_page->ID)) : ?>
                    <a class="read-more" href="/category/<?php echo $section_cat->slug; ?>">
                        <?php echo str_replace('%category%', $section_cat->name, get_field('section_home_read_more_stories_link', $section_page->ID)); ?>
                    </a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div><!-- stories -->