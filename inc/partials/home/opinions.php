<?php
/**
 * The loop for posts in the 'opinions' section of the home page.
 *
 * @package Morning Consult 2015
 */
?>

<div class="opinions">
    <div class="container">
        <h2 class="section-title"><?php the_field('home_opinions_section_title'); ?></h2>

        <div class="row">
            <div class="col-xs-12 col-sm-6 left">

                <?php
                    $home_opinions_args = array (
                        'post_type'              => 'mc_opinion',
                        'posts_per_page'         => '6',
                    );

                    $home_opinions = new WP_Query($home_opinions_args);

                    if( $home_opinions->have_posts() ): while( $home_opinions->have_posts() ): $home_opinions->the_post();

                    // End first column and start second column
                    if ($home_opinions->current_post === 3) :
                        echo '</div>';
                        echo '<div class="col-xs-12 col-sm-6 right">';
                    endif;
                ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class('excerpt'); ?>>
                        <?php if ($home_opinions->current_post === 1) : ?>
                            <?php the_post_thumbnail('large'); ?>
                        <?php endif; ?>

                        <header class="entry-header">
                            <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '">', '</a></h3>' ); ?>
                            <h4 class="author"><?php echo get_the_author_meta( 'first_name' ); ?> <?php echo get_the_author_meta( 'last_name' ); ?></h4>
                        </header>

                        <footer class="entry-footer meta">
                            <?php morning_consult_category_links(); ?> |
                            <a class="timeago" href="<?php echo get_permalink(); ?>">
                                <?php the_time( 'F j' ); ?>
                            </a>
                        </footer>
                    </article>
                <?php
                    endwhile; endif;
                    wp_reset_postdata();
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <a class="read-more" href="/opinions/">
                    Read more opinions >>
                </a>
            </div>
        </div>

    </div>
</div><!-- opinions -->
