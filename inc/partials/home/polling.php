<?php
/**
 * The loop for posts in the 'polling' section of the home page.
 *
 * @package Morning Consult 2015
 */
?>

<?php

    global $post;
    $home_page = $post;

    $home_polls_args = array (
        'post_type'              => 'mc_poll',
        'posts_per_page'         => '4',
    );

    $home_polls = new WP_Query($home_polls_args);

    if( $home_polls->have_posts() ): while( $home_polls->have_posts() ): $home_polls->the_post();
?>

        <?php // Most recent poll - first section ?>
        <?php if ($home_polls->current_post === 0) : ?>

            <div class="polling-featured">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8">
                            <?php
                                if (get_field('poll_featured_chart')) :
                                    echo do_shortcode(get_field('poll_featured_chart'));
                                elseif ( has_post_thumbnail() ) :
                                   the_post_thumbnail('large');
                                endif;
                            ?>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <h2 class="section-title"><?php the_field('home_polling_section_title', $home_page->ID); ?></h2>
                            <article id="post-<?php the_ID(); ?>" <?php post_class('excerpt'); ?>>

                                <header class="entry-header">
                                    <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '">', '</a></h3>' ); ?>
                                </header>

                                <div class="entry-content">
                                    <?php the_excerpt(); ?>
                                </div>

                                <footer class="entry-footer meta">
                                    <?php morning_consult_category_links(); ?>&nbsp;|&nbsp;
                                    <a class="timeago" href="<?php echo get_permalink(); ?>">
                                        <?php the_time( 'F j' ); ?>
                                    </a>
                                </footer>
                            </article>
                        </div>
                    </div>
                </div>
            </div>

        <?php else : ?>

            <?php // Opening of second section ?>
            <?php if ($home_polls->current_post === 1) : ?>

                <div class="polling-lower">
                    <div class="container">
                        <div class="row">

            <?php endif; ?>
                            <div class="col-xs-12 col-sm-4">
                                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                    <?php
                                        if (get_field('poll_featured_chart')) :
                                            echo do_shortcode(get_field('poll_featured_chart'));
                                        elseif ( has_post_thumbnail() ) :
                                           the_post_thumbnail('medium');
                                        endif;
                                    ?>

                                    <header class="entry-header">
                                        <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '">', '</a></h3>' ); ?>
                                    </header>
                                </article>
                            </div>
            <?php // Closing of second section ?>
            <?php if (($home_polls->current_post + 1) === $home_polls->post_count) : ?>
                        </div>
                    </div>
                </div><!-- polling-lower -->

            <?php endif; ?>

        <?php endif; ?>

<?php
    endwhile; endif;
    wp_reset_postdata();
?>
