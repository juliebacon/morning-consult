<?php
/**
 * The loop for posts in the 'stories' section of the home page.
 *
 * @package Morning Consult 2015
 */
?>

<?php

    global $post;
    $home_page = $post;
    $post_for_ad = $post;

    $home_featured_post_id = get_field('home_featured_post');
    $home_featured_args = array(
        'posts_per_page' => '1',
        'post_type' => 'post'
    );

    if ($home_featured_post_id) {
        $home_featured_args['post__in'] = array($home_featured_post_id);
        unset($home_featured_args['post_type']);
    }

    $home_featured = new WP_Query($home_featured_args);

    if( $home_featured->have_posts() ): while( $home_featured->have_posts() ): $home_featured->the_post();

    if (!$home_featured_post_id) {
        $home_featured_post_id = get_the_ID();
    }

    ?>

        <div class="featured <?php echo morning_consult_which_categories(); ?>">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <header class="entry-header">
                            <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                            <a class="btn btn-default btn-inverted read-more" href="<?php echo esc_url( get_permalink() ); ?>">Read more</a>
                        </header><!-- .entry-header -->
                    </div>
                </div>
            </div>
        </div>

        <?php
            $home_featured_story_image = morning_consult_get_featured_image_info();
            wp_localize_script( 'morning-consult-all-js', 'featured_story_image', $home_featured_story_image );
        ?>

    <?php endwhile; endif;
    wp_reset_postdata();

    $home_stories_args = array (
        'post_type'              => 'post',
        'posts_per_page'         => '5',
        'post__not_in'           => array($home_featured_post_id)
    );

    $home_stories = new WP_Query($home_stories_args);

    if( $home_stories->have_posts() ): while( $home_stories->have_posts() ): $home_stories->the_post();
?>

        <?php // Opening of second section ?>
        <?php if ($home_stories->current_post === 0) : ?>

            <div class="stories-lower">
                <div class="container">
                    <h2 class="section-title"><?php the_field('home_stories_section_title', $home_page->ID); ?></h2>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">

        <?php endif; ?>

                    <?php // end first column, start second column ?>
                    <?php if ($home_stories->current_post === 2) : ?>

                        </div>
                        <div class="col-xs-12 col-sm-4 middle-column">

                    <?php endif; ?>

                            <article id="post-<?php the_ID(); ?>" <?php post_class('excerpt'); ?>>
                                <?php if ($home_stories->current_post === 0) : ?>
                                    <?php the_post_thumbnail('large'); ?>
                                <?php endif; ?>

                                <header class="entry-header">
                                    <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '">', '</a></h3>' ); ?>
                                </header>

                                <div class="entry-content">
                                    <?php the_excerpt(); ?>
                                </div>

                                <footer class="entry-footer meta">
                                    <?php morning_consult_category_links(); ?> |
                                    <a class="timeago" href="<?php echo get_permalink(); ?>">
                                        <?php the_time( 'F j' ); ?>
                                    </a>
                                </footer>
                            </article>

        <?php // Closing of second section ?>
        <?php if (($home_stories->current_post + 1) === $home_stories->post_count) : ?>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <?php include(locate_template('inc/small-ad-area.php')); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <a class="read-more" href="/stories/">
                                Read more stories >>
                            </a>
                        </div>
                    </div>
                </div>
            </div><!-- stories-lower -->

        <?php endif; ?>

<?php
    endwhile; endif;
    wp_reset_postdata();
?>
