    <?php
        $author_id = get_the_author_meta('ID');
        $author_featured_image = get_field('author_archive_background_image', 'user_' . $author_id);

        if ($author_featured_image) :
            $author_featured_image = morning_consult_get_featured_image_info($author_featured_image['id']);
            wp_localize_script( 'morning-consult-all-js', 'featured_story_image', $author_featured_image );
        ?>

        <div class="featured">
            <div class="author-info">
                <div class="container">
                    <div class="col-sm-6">
                        <div class="content">
                            <h1 class="name"><?php echo get_the_author_meta( 'first_name' ); ?> <?php echo get_the_author_meta( 'last_name' ); ?></h1>
                            <?php
                                $author_twitter = strtolower(get_the_author_meta( 'twitter' ));
                                if ($author_twitter && strpos($author_twitter, 'twitter.com') !== false) :
                                    echo '<a class="twitter" target="_blank" href="' . $author_twitter . '">@' . substr($author_twitter, strlen('twitter.com/') + strpos($author_twitter, 'twitter.com/')) . '</a>';
                                elseif ($author_twitter) :
                                    echo '<a class="twitter" target="_blank" href="http://www.twitter.com/' . $author_twitter . '">@' . $author_twitter . '</a>';
                                endif;
                            ?>
                            <p class="job-title"><strong><?php the_field('author_job_title', 'user_' . $author_id); ?></strong></p>
                            <div class="bio hidden-xs">
                                <?php the_field('author_long_bio', 'user_' . $author_id); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container bio-mobile visible-xs-block">
            <?php the_field('author_long_bio', 'user_' . $author_id); ?>
        </div>

    <?php endif; ?>