<?php
/**
 * The loop for posts in the 'stories' portion of the section home page.
 *
 * @package Morning Consult 2015
 */
?>

<?php
    global $post;
    $section_page = $post;
    $section_cat = get_field('section_home_category');
?>

<div class="stories">
    <div class="container">
        <!-- <h2 class="section-title"><?php the_field('section_home_stories_section_title', $section_page->ID); ?></h2> -->
        <h2 class="section-title">Top <?php echo $section_cat->name; ?> Stories</h2>
        <div class="row">
            <div class="col-sm-4">

                <?php

                    $section_home_stories_args = array (
                        'post_type'              => 'post',
                        'posts_per_page'         => '6',
                        'post__not_in'           => array($section_home_featured_post_id)
                    );

                    if ($section_cat) {
                        $section_home_stories_args['cat'] = $section_cat->term_id;
                    }

                    $section_home_stories = new WP_Query($section_home_stories_args);

                    if( $section_home_stories->have_posts() ): while( $section_home_stories->have_posts() ): $section_home_stories->the_post();

                    if (($section_home_stories->current_post % 2) === 0 && $section_home_stories->current_post > 0) :
                        echo '</div>';
                        echo '<div class="col-sm-4">';
                    endif;

                ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class('excerpt'); ?>>

                        <header class="entry-header">
                            <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '">', '</a></h3>' ); ?>
                        </header>

                        <div class="entry-content">
                            <?php the_excerpt(); ?>
                        </div>

                        <footer class="entry-footer meta">
                            <?php morning_consult_category_links(); ?> |
                            <a class="timeago" href="<?php echo get_permalink(); ?>">
                                <?php the_time( 'F j' ); ?>
                            </a>
                        </footer>
                    </article>

                <?php
                    endwhile; endif;
                    wp_reset_postdata();
                ?>
            </div>
        </div><!-- row -->
        <div class="row">
            <div class="col-xs-12">
                <?php if (get_field('section_home_read_more_stories_link', $section_page->ID)) : ?>
                    <a class="read-more" href="/category/<?php echo $section_cat->slug; ?>">
                        <?php echo str_replace('%category%', $section_cat->name, get_field('section_home_read_more_stories_link', $section_page->ID)); ?>
                    </a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div><!-- stories -->