<?php
/**
 * The top portion of the section home page
 *
 * @package Morning Consult 2015
 */
?>

<?php $section_cat = get_field('section_home_category'); ?>

<div class="brief-polling <?php echo $section_cat->slug; ?>">
    <div class="container">
        <div class="row">

            <?php
                /*
                *   First Column - Daily Brief
                 */
                global $post;
                $section_page = $post;
                $post_for_ad = $section_page;

                $section_home_brief_args = array (
                    'post_type'              => 'mc_brief',
                    'posts_per_page'         => 1,
                );

                if ($section_cat) {
                    $section_home_brief_args['cat'] = $section_cat->term_id;
                }

                $section_home_brief = new WP_Query($section_home_brief_args);

                if( $section_home_brief->have_posts() ): while( $section_home_brief->have_posts() ): $section_home_brief->the_post();
            ?>
                <div class="col-xs-12 col-sm-4 left-column">

                    <?php
                        if (get_field('section_home_daily_brief_title', $section_page->ID)) :
                            echo '<h2 class="section-title">' . str_replace('%category%', $section_cat->name, get_field('section_home_daily_brief_title', $section_page->ID)) . '</h2>';
                        endif;
                    ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class('excerpt'); ?>>

                        <header class="entry-header">
                            <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '">', '</a></h3>' ); ?>
                        </header>

                        <div class="entry-content">
                            <?php the_excerpt(); ?>
                        </div>

                        <footer class="entry-footer meta">
                            <div class="meta">
                                <?php morning_consult_category_links(); ?> |
                                <a class="timeago" href="<?php echo get_permalink(); ?>">
                                    <?php the_time( 'F j' ); ?>
                                </a>
                            </div>
                            <a class="btn btn-default read-more" href="/briefs/category/<?php echo $section_cat->slug; ?>">Read more</a>
                        </footer>

                        <?php if (get_field('section_home_subscribe_link', $section_page->ID)) : ?>
                            <div class="meta">
                                <p><a href="/subscribe">
                                <?php echo str_replace('%category%', $section_cat->name, get_field('section_home_subscribe_link', $section_page->ID)); ?>
                                </a></p>
                            </div>
                        <?php endif; ?>
                    </article>
                </div>

            <?php
                endwhile; endif;
                wp_reset_postdata();
            ?>

            <?php
                $section_home_poll_args = array (
                    'post_type'              => 'mc_poll',
                    'posts_per_page'         => 1,
                );

                if ($section_cat) {
                    $section_home_poll_args['cat'] = $section_cat->term_id;
                }

                $section_home_poll_more_args = $section_home_poll_args;
                $section_home_poll_more_args['posts_per_page'] = 3;
                $section_home_poll_more_args['offset'] = 1;
                $section_home_poll_more = new WP_Query($section_home_poll_more_args);
                $section_home_poll_html = "";

                if( $section_home_poll_more->have_posts() ):
                    $section_home_poll_html = '<div class="more-polls"><h3>More polls:</h3>';
                    while( $section_home_poll_more->have_posts() ): $section_home_poll_more->the_post();
                        $section_home_poll_html .= '<a href="' . get_permalink() . '">' . get_the_title() . '</a>';
                    endwhile;
                    $section_home_poll_html .= '</div>';
                endif;
                wp_reset_postdata();

                $section_home_poll = new WP_Query($section_home_poll_args);

                if( $section_home_poll->have_posts() ): while( $section_home_poll->have_posts() ): $section_home_poll->the_post();

                $poll_featured_chart = do_shortcode(get_field('poll_featured_chart'));
            ?>
                <div class="col-xs-12 col-sm-4 pull-right">
                    <?php
                        if (get_field('page_small_ad_unit', $section_page->ID) && get_field('page_small_ad_unit', $section_page->ID) !== "None") {
                            include(locate_template('inc/small-ad-area.php'));
                        } else {
                            echo $poll_featured_chart;
                        }
                    ?>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <?php
                        if (get_field('section_home_polling_title', $section_page->ID)) :
                            echo '<h2 class="section-title">' . str_replace('%category%', $section_cat->name, get_field('section_home_polling_title', $section_page->ID)) . '</h2>';
                        endif;
                    ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class('excerpt'); ?>>

                        <header class="entry-header">
                            <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '">', '</a></h3>' ); ?>
                        </header>

                        <div class="entry-content">
                            <?php the_excerpt(); ?>
                        </div>

                        <footer class="entry-footer meta">
                            <?php morning_consult_category_links(); ?> |
                            <a class="timeago" href="<?php echo get_permalink(); ?>">
                                <?php the_time( 'F j' ); ?>
                            </a>
                        </footer>
                    </article>

                    <?php echo $section_home_poll_html; ?>
                </div><!-- col-sm-4 -->
            <?php
                endwhile; endif;
                wp_reset_postdata();
            ?>
        </div>
    </div>
</div>
