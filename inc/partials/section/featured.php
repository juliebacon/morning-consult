<?php
/**
 * The featured story on a section home page.
 *
 * @package Morning Consult 2015
 */
?>

<?php

    $section_cat = get_field('section_home_category');

    $section_featured_post_id = get_field('section_home_featured_post');

    $section_featured_args = array (
        'post_type'              => 'post',
        'posts_per_page'         => '1',
    );

    if ($section_featured_post_id) {
        $home_featured_args['post__in'] = array($section_featured_post_id);
        unset($home_featured_args['post_type']);
    }

    if ($section_cat) {
        $section_featured_args['cat'] = $section_cat->term_id;
    }

    $section_featured = new WP_Query($section_featured_args);

    if( $section_featured->have_posts() ): while( $section_featured->have_posts() ): $section_featured->the_post();

    if (!isset($section_home_featured_post_id)) {
        $section_home_featured_post_id = get_the_ID();
    }

?>

        <div class="featured <?php echo $section_cat->slug; ?>">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <header class="entry-header">
                            <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                            <a class="btn btn-default btn-inverted read-more" href="<?php echo esc_url( get_permalink() ); ?>">Read more</a>
                        </header><!-- .entry-header -->
                    </div>
                </div>
            </div>
        </div>

        <?php
            $section_home_featured_story_image = morning_consult_get_featured_image_info();
            wp_localize_script( 'morning-consult-all-js', 'featured_story_image', $section_home_featured_story_image );
        ?>

<?php
    endwhile; endif;
    wp_reset_postdata();
?>
