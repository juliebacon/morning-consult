<?php
/**
 * The loop for posts in the 'opinions' portion of the section home page.
 *
 * @package Morning Consult 2015
 */
?>

<?php
    global $post;
    $section_page = $post;
    $section_cat = get_field('section_home_category');
?>

<div class="opinions">
    <div class="container">
        <!-- <h2 class="section-title"><?php the_field('section_home_opinions_section_title', $section_page->ID); ?></h2> -->
        <h2 class="section-title">Top <?php echo $section_cat->name; ?> Opinions</h2>
        <div class="row">
            <div class="col-sm-6">

                <?php

                    $section_home_opinions_args = array (
                        'post_type'              => 'mc_opinion',
                        'posts_per_page'         => '6',
                    );

                    if ($section_cat) {
                        $section_home_opinions_args['cat'] = $section_cat->term_id;
                    }

                    $section_home_opinions = new WP_Query($section_home_opinions_args);

                    if( $section_home_opinions->have_posts() ): while( $section_home_opinions->have_posts() ): $section_home_opinions->the_post();

                    if ($section_home_opinions->current_post === 3 ) :
                        echo '</div>';
                        echo '<div class="col-sm-6">';
                    endif;

                ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class('excerpt'); ?>>

                        <header class="entry-header">
                            <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '">', '</a></h3>' ); ?>
                            <h4 class="author"><?php echo get_the_author_meta( 'first_name' ); ?> <?php echo get_the_author_meta( 'last_name' ); ?></h4>
                        </header>

                        <footer class="entry-footer meta">
                            <?php morning_consult_category_links(); ?> |
                            <a class="timeago" href="<?php echo get_permalink(); ?>">
                                <?php the_time( 'F j' ); ?>
                            </a>
                        </footer>
                    </article>

                <?php
                    endwhile; endif;
                    wp_reset_postdata();
                ?>
            </div>
        </div><!-- row -->
        <div class="row">
            <div class="col-xs-12">
                <?php if (get_field('section_home_read_more_opinions_link', $section_page->ID)) : ?>
                    <a class="read-more" href="/opinions/category/<?php echo $section_cat->slug; ?>">
                        <?php echo str_replace('%category%', $section_cat->name, get_field('section_home_read_more_opinions_link', $section_page->ID)); ?>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div><!-- opinions -->