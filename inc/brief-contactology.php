<?php
add_action( 'wp_ajax_mc_create_campaign', 'mc_create_campaign_callback' );

function mc_create_campaign_callback() {
    $url_query_parts = array();
    $url_query = parse_url($_POST['url'], PHP_URL_QUERY);
    parse_str($url_query,$url_query_parts);
    $post_id = $url_query_parts['post'];

    $system = (array_key_exists('emailsystem', $_POST) && $_POST['emailsystem']) ? $_POST['emailsystem'] : "contactology";

    $sender_email = get_field('brief_sender_email_address',$post_id);
    $sender_name = get_field('brief_sender_name',$post_id);
    $email_content = get_field('brief_email_content',$post_id);

    // Inline CSS
    $css_inliner_response = wp_remote_post(
        'http://premailer.dialect.ca/api/0.1/documents',
        array(
            'timeout' => 15,
            'body' => array(
                'html' => $email_content,
                'base_url' => 'morningconsult.com'
            )
        )
    );

    if (is_wp_error($css_inliner_response)) {
        global $current_user;
        get_currentuserinfo();
        wp_send_json_error( "While creating the campaign for the Morning Brief (Post ID {$post_id}), there was a problem with the CSS inliner tool. The error message was:\n\n " . $css_inliner_response->get_error_message() );
    } else {
        $css_inliner_response_body = json_decode($css_inliner_response['body'],true);
        $html_link = $css_inliner_response_body['documents']['html'];
        $inlined_html_response = wp_remote_get($html_link);
        $email_content = $inlined_html_response['body'];
    }

    remove_filter('the_title', 'wptexturize');
    remove_filter('the_title', 'convert_chars');

    if ($system === "inboxfirst") {
        mc_create_campaign_inboxfirst($post_id, $sender_email, $sender_name, $email_content);
    } else {
        mc_create_campaign_contactology($post_id, $sender_email, $sender_name, $email_content);
    }
}

function mc_create_campaign_contactology($post_id, $sender_email, $sender_name, $email_content) {
    $api_key = get_field('brief_contactology_api_key','option');

    if (!$api_key) {
        wp_send_json_error( 'API key not set' );
    }

    $contactology = new Contactology($api_key);

    $list_id = get_field('brief_list_id',$post_id);

    if (!$list_id) {
        wp_send_json_error( 'No List ID specified' );
    }

    $result = $contactology->Campaign_Create_Standard(
        array(
            'list' => $list_id
        ),
        get_the_title($post_id), // campaign name
        get_the_title($post_id), // subject
        $sender_email,
        $sender_name,
        array(
            'html' => $email_content
        ),
        array(
            'generateTextFromHtml' => true
        )
    );

    wp_send_json_success( $result );
}

function mc_create_campaign_inboxfirst($post_id, $sender_email, $sender_name, $email_content) {
    $api_key = get_field('brief_inboxfirst_api_key','option');
    $org_id = get_field('brief_inboxfirst_org_id','option');
    $list_id = get_field('brief_inboxfirst_list_id',$post_id);

    if (!$api_key || !$org_id) {
        wp_send_json_error( 'API credentials not set' );
    }

    if (!$list_id) {
        wp_send_json_error( 'No List ID specified' );
    }

    $http_args = array(
        'headers' => array(
            'Authorization' => 'Basic ' . base64_encode( $org_id . ':' . $api_key )
        ),
        'httpversion' => '1.1',
        'body' => json_encode(array(
            'campaign' => array(
                'name' => substr(get_the_title($post_id),0,150),
                'campaign_contents_attributes' => array(
                    array(
                        'content_attributes' => array(
                            'html' => $email_content,
                            'format' => 'html',
                            'subject' => substr(get_the_title($post_id),0,150)
                        )
                    )
                ),
                'dispatch_attributes' => array(
                    'from_name' => $sender_name,
                    'from_email' => $sender_email
                )
            )
        ))
    );

    $response = wp_remote_post(
        "http://if.inboxfirst.com/ga/api/v2/mailing_lists/" . $list_id . "/campaigns",
        $http_args
    );

    if (!is_wp_error( $response )) {
        $response_body = json_decode($response['body']);
        if ($response_body->success) {
            wp_send_json_success( $response_body->data->id );
        } else {
            $error_message = $response_body->error_message;
            $error_message .= " (error code " . $response_body->error_code . ")";
            wp_send_json_error( $error_message );
        }
    } else {
        wp_send_json_error( $response->get_error_message() );
    }
}