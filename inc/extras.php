<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package Morning Consult 2015
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function morning_consult_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	return $classes;
}
add_filter( 'body_class', 'morning_consult_body_classes' );

function morning_consult_get_featured_image_info($id = null) {
    if ( !is_null($id) || has_post_thumbnail() ) {
        $image_id = is_null($id) ? get_post_thumbnail_id() : $id;
        $image_url = wp_get_attachment_url( $image_id );
        $image_caption = get_post_field('post_excerpt', $image_id, 'display');
        return array(
            'url' => $image_url,
            'caption' => $image_caption
        );
    }

    return null;
}

// Grab the slug of the first category which is in the list of main categories
function morning_consult_which_category($post_id = null) {
	global $post;
    if (is_null($post_id)) {
        $post_id = $post->ID;
    }
	$main_categories = array('tech', 'health', 'energy', 'finance', 'congress', 'campaigns');
	$categories = get_the_category($post_id);

	foreach($categories as $category) {
		if (in_array($category->slug, $main_categories)) {
			return $category->slug;
		}
	}

	return null;
}

// return the links of all the categories which are in the list of main categories
function morning_consult_category_links($post_id = null) {
    global $post;
    $html = "";
    if (is_null($post_id)) {
        $post_id = $post->ID;
    }

    $main_categories = array('tech', 'health', 'energy', 'finance', 'congress', 'campaigns');
    $all_categories = get_the_category($post_id);

    foreach($all_categories as $category) {
        if (in_array($category->slug, $main_categories)) {
            $html .= '<a class="category ' . $category->slug . '" href="/' . $category->slug . '">' . $category->name . '</a>, ';
        }
    }

    $html = trim($html,', ');

    echo $html;
}

// return the slugs, space separated, of all the categories which are in the list of main categories
function morning_consult_which_categories($post_id = null) {
    global $post;
    if (is_null($post_id)) {
        $post_id = $post->ID;
    }
    $main_categories = array('tech', 'health', 'energy', 'finance', 'congress', 'campaigns');
    $categories = get_the_category($post_id);
    $category_slugs = array();

    foreach($categories as $category) {
        if (in_array($category->slug, $main_categories)) {
            $category_slugs[] = $category->slug;
        }
    }

    return implode(' ', $category_slugs);
}

// Custom 'more' tag for excerpts
add_filter('excerpt_more', 'morning_consult_custom_excerpt');
function morning_consult_custom_excerpt($more) {
    return '';
}

if ( version_compare( $GLOBALS['wp_version'], '4.1', '<' ) ) :
	/**
	 * Filters wp_title to print a neat <title> tag based on what is being viewed.
	 *
	 * @param string $title Default title text for current view.
	 * @param string $sep Optional separator.
	 * @return string The filtered title.
	 */
	function morning_consult_wp_title( $title, $sep ) {
		if ( is_feed() ) {
			return $title;
		}

		global $page, $paged;

		// Add the blog name
		$title .= get_bloginfo( 'name', 'display' );

		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title .= " $sep $site_description";
		}

		// Add a page number if necessary:
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title .= " $sep " . sprintf( __( 'Page %s', 'morning-consult' ), max( $paged, $page ) );
		}

		return $title;
	}
	add_filter( 'wp_title', 'morning_consult_wp_title', 10, 2 );

	/**
	 * Title shim for sites older than WordPress 4.1.
	 *
	 * @link https://make.wordpress.org/core/2014/10/29/title-tags-in-4-1/
	 * @todo Remove this function when WordPress 4.3 is released.
	 */
	function morning_consult_render_title() {
		?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php
	}
	add_action( 'wp_head', 'morning_consult_render_title' );
endif;

add_filter('upload_mimes', 'morning_consult_allow_svg');
function morning_consult_allow_svg($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

add_filter("gform_submit_button", "morning_consult_alter_form_submit_button", 10, 2);
function morning_consult_alter_form_submit_button($button, $form){
    if ($form["button"]["type"] == "text") {
        // (class=) followed by (single or double quote) followed by (anything that is not a single or double quote)
        $pattern = '/(class=)(\'|")([^\'"]+)/';
        $replacement = '${1}${2}${3} btn btn-default';
        $newbutton = preg_replace($pattern, $replacement, $button);
        if ( !is_null($newbutton) ) {
            $button = $newbutton;
        }
    }
    return $button;
}

add_filter("gform_init_scripts_footer", create_function("","return true;"));
add_filter( 'gform_confirmation_anchor', create_function("","return false;"));

add_filter("gform_ajax_spinner_url", "morning_consult_gf_spinner_url", 10, 2);
function morning_consult_gf_spinner_url($image_src, $form){
    return get_template_directory_uri() . '/img/ajax-loader.png';
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

// Lets us use this URL format: example.com/custom-post-type/category/category-name/
add_action('init', 'category_cpt_rewrites');
function category_cpt_rewrites() {
    $custom_post_types = array('opinions' => 'mc_opinion', 'briefs' => 'mc_brief', 'polls' => 'mc_poll');
    foreach ( $custom_post_types as $key => $post_type ) {
        $rule = '^' . $key . '\/category\/(.+?)\/?(page)?\/?(\d*)?\/?$';
        $rewrite = 'index.php?post_type=' . $post_type . '&category_name=$matches[1]&paged=$matches[3]';
        add_rewrite_rule($rule,$rewrite,'top');
    }
}

add_action('pre_get_posts','morning_consult_archive_filter');
function morning_consult_archive_filter($query) {
	if ( !is_admin() && $query->is_main_query() ) {
		if ($query->is_search() || $query->is_author()) {
			$query->set('post_type', array( 'post', 'mc_brief', 'mc_poll', 'mc_opinion' ) );
		}
	}
}

add_filter( 'excerpt_length', 'morning_consult_excerpt_length', 20 );
function morning_consult_excerpt_length( $length ) {
	return 30;
}

add_filter( 'user_contactmethods', 'morning_consult_user_contact_methods' );
function morning_consult_user_contact_methods( $user_contact ) {

    /* Add user contact methods */
    $user_contact['twitter'] = __( 'Twitter Username' );

    return $user_contact;
}

/* Don't display briefs on author archive pages */
add_action( 'pre_get_posts', 'mc_remove_briefs_from_author_archive' );
function mc_remove_briefs_from_author_archive( $query ) {
    if (!is_admin() && is_main_query() && is_author() && $query->is_author) {
        $post_types = $query->get('post_type');

        foreach ($post_types as $key => $value) {
            if ($value === 'mc_brief') {
                unset($post_types[$key]);
            }
        }

        $query->set('post_type',$post_types);
    }
}

add_filter( 'request', 'mc_rewrite_filter_request' );
function mc_rewrite_filter_request( $vars ) {
    if( isset($vars['email_print']) ) {
        $vars['email_print'] = true;
    }
    return $vars;
}

add_action( 'init', 'mc_rewrite_add_rewrites' );
function mc_rewrite_add_rewrites() {
    add_rewrite_endpoint( 'email_print', EP_PERMALINK );
}

add_filter( 'template_include', 'mc_brief_email_template', 99 );
function mc_brief_email_template( $template ) {
    if( is_singular() && get_query_var( 'email_print' ) ) {
        $new_template = locate_template( array( 'page-brief-email.php' ) );
        if ( '' != $new_template ) {
            return $new_template ;
        }
    }
    return $template;
}
