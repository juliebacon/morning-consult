<?php
    $wide_ad_2 = get_field('page_wide_ad_unit_2');

    if ($wide_ad_2 && $wide_ad_2 === "Adsense") : ?>

        <div class="wide-ad-area wide-ad-area-2 adsense page-specific">
            <div class="ad-wrap">
                <?php the_field('page_wide_ad_unit_2_code'); ?>
            </div>
        </div>

    <?php elseif ($wide_ad_2 && $wide_ad_2 === "Custom (sidebar)") : ?>

        <?php if(is_active_sidebar('wide-ad-area')) : ?>
            <div class="wide-ad-area wide-ad-area-2 sidebar page-specific">
                <div class="ad-wrap">
                    <?php dynamic_sidebar( 'wide-ad-area' ); ?>
                </div>
            </div>
        <?php endif; ?>

    <?php elseif ($wide_ad_2 && $wide_ad_2 === "Custom (upload)") : ?>

        <div class="wide-ad-area wide-ad-area-2 custom-link">
            <div class="ad-wrap">
                <a target="_blank" href="<?php the_field('page_wide_ad_unit_2_link'); ?>">
                    <img src="<?php the_field('page_wide_ad_unit_2_image'); ?>" alt="advertisement">
                </a>
            </div>
        </div>

    <?php elseif (!$wide_ad_2 || $wide_ad_2 === "Site Default") : ?>

        <?php $wide_ad_2_site = get_field('wide_ad_unit_2_type','option'); ?>

        <?php if ($wide_ad_2_site && $wide_ad_2_site === "Adsense") : ?>

            <div class="wide-ad-area wide-ad-area-2 adsense site-default">
                <div class="ad-wrap">
                    <?php the_field('wide_ad_unit_2_code','option'); ?>
                </div>
            </div>

        <?php elseif ($wide_ad_2_site && $wide_ad_2_site === "Custom (Sidebar)") : ?>

            <?php if(is_active_sidebar('wide-ad-area')) : ?>
                <div class="wide-ad-area wide-ad-area-2 sidebar site-default">
                    <div class="ad-wrap">
                        <?php dynamic_sidebar( 'wide-ad-area' ); ?>
                    </div>
                </div>
            <?php endif; ?>

        <?php endif; ?>

    <?php endif; ?>