<?php
    if (!$post_for_ad) {
        $post_for_ad = $post;
    }

    $small_ad = get_field('page_small_ad_unit',$post_for_ad);
    if ($small_ad && $small_ad === "Adsense") : ?>

        <div class="small-ad-area adsense page-specific">
            <div class="ad-wrap">
                <?php the_field('page_small_ad_unit_code',$post_for_ad); ?>
            </div>
        </div>

    <?php elseif ($small_ad && $small_ad === "Custom (sidebar)") : ?>

        <?php if(is_active_sidebar('small-ad-area')) : ?>
            <div class="small-ad-area sidebar page-specific">
                <div class="ad-wrap">
                    <?php dynamic_sidebar( 'small-ad-area' ); ?>
                </div>
            </div>
        <?php endif; ?>

    <?php elseif ($small_ad && $small_ad === "Custom (upload)") : ?>

        <div class="small-ad-area custom-link">
            <div class="ad-wrap">
                <a target="_blank" href="<?php the_field('page_small_ad_unit_link',$post_for_ad); ?>">
                    <img src="<?php the_field('page_small_ad_unit_image',$post_for_ad); ?>" alt="advertisement">
                </a>
            </div>
        </div>

    <?php elseif (!$small_ad || $small_ad === "Site Default") : ?>

        <?php $small_ad_site = get_field('small_ad_unit_type','option'); ?>

        <?php if ($small_ad_site && $small_ad_site === "Adsense") : ?>

            <div class="small-ad-area adsense site-default">
                <div class="ad-wrap">
                    <?php the_field('small_ad_unit_code','option'); ?>
                </div>
            </div>

        <?php elseif ($small_ad_site && $small_ad_site === "Custom (Sidebar)") : ?>

            <?php if(is_active_sidebar('small-ad-area')) : ?>
                <div class="small-ad-area sidebar site-default">
                    <div class="ad-wrap">
                        <?php dynamic_sidebar( 'small-ad-area' ); ?>
                    </div>
                </div>
            <?php endif; ?>

        <?php endif; ?>

    <?php endif; ?>