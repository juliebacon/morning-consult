<?php

function mc_save_post_content( $post_id ) {
    $post_type = get_post_type($post_id);

    if( empty($_POST['acf']) ) {
        return;
    }

    if( !empty($GLOBALS['wp_embed']) ) {
        remove_filter( 'acf_the_content', array( $GLOBALS['wp_embed'], 'run_shortcode' ), 8 );
        remove_filter( 'acf_the_content', array( $GLOBALS['wp_embed'], 'autoembed' ), 8 );
    }
    remove_filter( 'acf_the_content', 'capital_P_dangit', 11 );
    remove_filter( 'acf_the_content', 'wptexturize' );
    remove_filter( 'acf_the_content', 'convert_smilies' );
    remove_filter( 'acf_the_content', 'convert_chars' );
    remove_filter( 'acf_the_content', 'wpautop' );
    remove_filter( 'acf_the_content', 'shortcode_unautop' );
    remove_filter( 'acf_the_content', 'prepend_attachment' );
    remove_filter( 'acf_the_content', 'do_shortcode', 11);

    // Morning Briefs
    if ($post_type === "mc_brief" && have_rows('morning_brief_content_sections')) :
        $new_brief_content = "";

        while ( have_rows('morning_brief_content_sections') ) : the_row();
            $brief_layout = get_row_layout();

            if ($brief_layout === "todays_washington_brief") :
                $new_brief_content .= "<h2>Today's Washington Brief</h2>\n";
                if (have_rows('washington_paragraphs')) : while (have_rows('washington_paragraphs')) : the_row();
                    $new_brief_content .= "<p>" . get_sub_field('paragraph') . "</p>\n";
                endwhile; endif;
            endif;

            if ($brief_layout === "todays_business_brief") :
                $new_brief_content .= "<h2>Today's Business Brief</h2>\n";
                if (have_rows('business_paragraphs')) : while (have_rows('business_paragraphs')) : the_row();
                    $new_brief_content .= "<p>" . get_sub_field('paragraph') . "</p>\n";
                endwhile; endif;
            endif;

            if ($brief_layout === "todays_chart_review") :
                $new_brief_content .= "<h2>" . get_sub_field('chart_title') . "</h2>\n";
                $new_brief_content .= "<div>" . get_sub_field('chart_headline') . "</div>\n";
                $new_brief_content .= "<div style='margin-top: 10px; margin-bottom: 10px;'><img src='" . get_sub_field('chart_image') . "'></div>\n";
                $new_brief_content .= "<div style='clear: both;'></div>\n";
            endif;

            if ($brief_layout === "calendar") :
                $new_brief_content .= "<h2>" . get_sub_field('calendar_title') . "</h2>\n";
                if (have_rows('dates')) : while (have_rows('dates')) : the_row();
                    $new_brief_content .= "<p>";
                    $new_brief_content .= "<strong>" . get_sub_field('day') . "</strong><br>\n";
                    $new_brief_content .= get_sub_field('events') . "\n";
                    $new_brief_content .= "</p>\n";
                endwhile; endif;
            endif;

            if ($brief_layout === "general_section_title") :
                $new_brief_content .= "<h2>" . get_sub_field('section_title') . "</h2>\n";
            endif;

            if ($brief_layout === "general_section_paragraph") :
                $new_brief_content .= "<p>";
                    if (get_sub_field('make_bold')) :
                        $new_brief_content .= "<strong>" . get_sub_field('text') . "</strong>";
                    else :
                        $new_brief_content .= get_sub_field('text');
                    endif;
                $new_brief_content .= "</p>";
            endif;

            if ($brief_layout === "html_content") :
                $new_brief_content .= get_sub_field('content');
            endif;

            if ($brief_layout === "image_ad") :
                $new_brief_content .= '<a href="' . get_sub_field('ad_link') . '">';
                $new_brief_content .= '<img src="' . get_sub_field('ad_image') . '">';
                $new_brief_content .= '</a>';
                $new_brief_content .= "<p>&nbsp;</p>\n";
            endif;

        endwhile;

        wp_update_post( array(
            'ID' => $post_id,
            'post_content' => $new_brief_content
        ) );
    endif;
    wp_reset_postdata();

    // Articles, Opinions, Polls
    if( have_rows('post_content_sections') ):

        $new_content = "";

        while ( have_rows('post_content_sections') ) : the_row();
            $layout = get_row_layout();
            // TODO: change content layout for emails
            switch ($layout) {
                default:
                    ob_start();
                    locate_template('inc/partials/single/' . $layout . '.php', true, false);
                    $new_content .= ob_get_contents();
                    ob_end_clean();
                    break;
            }

        endwhile;

        wp_update_post( array(
            'ID' => $post_id,
            'post_content' => $new_content
        ) );

    endif;
    wp_reset_postdata();

    if( !empty($GLOBALS['wp_embed']) ) {
        add_filter( 'acf_the_content', array( $GLOBALS['wp_embed'], 'run_shortcode' ), 8 );
        add_filter( 'acf_the_content', array( $GLOBALS['wp_embed'], 'autoembed' ), 8 );
    }
    add_filter( 'acf_the_content', 'capital_P_dangit', 11 );
    add_filter( 'acf_the_content', 'wptexturize' );
    add_filter( 'acf_the_content', 'convert_smilies' );
    add_filter( 'acf_the_content', 'convert_chars' );
    add_filter( 'acf_the_content', 'wpautop' );
    add_filter( 'acf_the_content', 'shortcode_unautop' );
    add_filter( 'acf_the_content', 'prepend_attachment' );
    add_filter( 'acf_the_content', 'do_shortcode', 11);


    // Morning Brief Email HTML
    if ($post_type === "mc_brief" && get_field('brief_overwrite_email')) {
        $brief_content = get_post( $post_id, 'OBJECT', 'display' );
        $brief_content = apply_filters('the_content', $brief_content->post_content);
        $html_content = get_field('brief_email_template','option');
        $title = get_the_title($post_id);
        $category_slug = morning_consult_which_category($post_id);
        $brief_byline = "<p>" . get_field('morning_brief_byline') . "</p>";

        $html_content = str_replace('{{byline}}', $brief_byline, $html_content);
        $html_content = str_replace('{{content}}', $brief_content, $html_content);
        $html_content = str_replace('{{title}}', $title, $html_content);
        $html_content = str_replace('{{mc_category_slug}}', $category_slug, $html_content);
        $html_content = str_replace('{{mc_category_name}}', ucwords($category_slug), $html_content);
        $html_content = str_replace('{{mc_theme_url}}', get_template_directory_uri(), $html_content);
        $html_content = str_replace('{{site_url}}', get_site_url(), $html_content);

        update_field('brief_email_content', $html_content);
    }

}

add_action('acf/save_post', 'mc_save_post_content', 100);