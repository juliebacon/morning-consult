<?php

if (class_exists("GFForms")) {
    GFForms::include_feed_addon_framework();

    class MorningConsultInboxFirstGravityForms extends GFFeedAddOn {

        protected $_version = "1.0";
        protected $_min_gravityforms_version = "1.7.9999";
        protected $_slug = "mc_inboxfirst";
        protected $_path = "wp-content/themes/morning-consult/inc/inboxfirst-gravity-forms.php";
        protected $_full_path = __FILE__;
        protected $_title = "InboxFirst Add-On";
        protected $_short_title = "InboxFirst Add-On";
        protected $http_args;
        protected $inboxfirst_url = "http://if.inboxfirst.com/ga/api/v2/";

        public function init(){
            parent::init();
            $api_key = $this->get_plugin_setting("mc_inboxfirst_api_key");
            $org_id = $this->get_plugin_setting("mc_inboxfirst_org_id");
            if ($api_key && $org_id) {
                $this->http_args = array(
                    'headers' => array(
                        'Authorization' => 'Basic ' . base64_encode( $org_id . ':' . $api_key )
                    ),
                    'httpversion' => '1.1'
                );
            }
        }

        public function get_http_args() {
            return $this->http_args;
        }

        public function get_lists() {
            $lists_for_feed = array(
                array(
                    'label' => 'No List',
                    'value' => '0'
                )
            );

            $get_lists_response = wp_remote_get( $this->inboxfirst_url . "/mailing_lists", $this->http_args );

            if ( !is_wp_error( $get_lists_response ) ) {

                $lists = json_decode($get_lists_response['body']);

                if ($lists->success) {

                    foreach ($lists->data as $listobj) {
                        $lists_for_feed[] = array(
                            'label' => $listobj->name,
                            'value' => $listobj->id
                        );
                    }
                } else {
                    echo "InboxFirst API Error Code: " . $get_lists_response->error_code . "<br>";
                    echo "InboxFirst API Error Message: " . $get_lists_response->error_message . "<br>";
                }

            } else {
                echo $get_lists_response->get_error_message();
            }

            return $lists_for_feed;
        }

        public function get_fields() {
            $fields_for_feed = array();

            $get_fields_response = wp_remote_get( $this->inboxfirst_url . "/custom_fields", $this->http_args );

            if ( !is_wp_error( $get_fields_response ) ) {

                $fields = json_decode($get_fields_response['body']);

                if ($fields->success) {
                    foreach ($fields->data as $fieldobj) {
                        $fields_for_feed[] = array(
                            'label' => $fieldobj->name,
                            'name' => $fieldobj->id
                        );
                    }
                    update_option( $this->_slug . "_custom_fields", $fields->data );
                } else {
                    echo "InboxFirst API Error Code: " . $get_fields_response->error_code . "<br>";
                    echo "InboxFirst API Error Message: " . $get_fields_response->error_message . "<br>";
                }

            } else {
                echo $get_fields_response->get_error_message();
            }

            return $fields_for_feed;
        }

        public function feed_settings_fields() {
            if (!$this->http_args) {
                return array();
            }

            $lists = $this->get_lists();
            $fields = $this->get_fields();

            return array(
                array(
                    "title"  => "InboxFirst Settings",
                    "fields" => array(
                        array(
                            "label"   => "Feed name",
                            "type"    => "text",
                            "name"    => "feedName",
                            "class"   => "small"
                        ),
                        array(
                            "label"   => "List",
                            "type"    => "select",
                            "name"    => "mc_inboxfirst_subscribe_list",
                            "choices" => $lists
                        ),
                        array(
                            "name" => "mappedFields",
                            "label" => "Map Fields",
                            "type" => "field_map",
                            "field_map" => $fields
                        ),
                        array(
                            "name" => "condition",
                            "label" => "Condition",
                            "type" => "feed_condition",
                            "checkbox_label" => "Enable Condition",
                            "instructions" => "Process this feed if"
                        )
                    )
                )
            );
        }

        public function plugin_settings_fields() {
            return array(
                array(
                    "title"  => "InboxFirst Add-On Settings",
                    "fields" => array(
                        array(
                            "name"    => "mc_inboxfirst_org_id",
                            "label"   => "InboxFirst Organization ID",
                            "type"    => "text",
                            "class"   => "small"
                        ),
                        array(
                            "name"    => "mc_inboxfirst_api_key",
                            "label"   => "InboxFirst API Key",
                            "type"    => "text",
                            "class"   => "small"
                        )
                    )
                )
            );
        }

        public function process_feed($feed, $entry, $form){
            if (!$feed['meta']['mc_inboxfirst_subscribe_list'] || !$feed['is_active']) {
                return;
            }

            $listid = $feed['meta']['mc_inboxfirst_subscribe_list'];

            $subscriber = array(
                'mailing_list_id' => $listid,
                'status' => 'active',
                'subscribe_time' => date('c'),
                'custom_fields' => array()
            );

            // get the email address
            foreach ($form['fields'] as $field) {
                if (is_array($field)) {
                    if ($field['type'] === "email" && !$field['adminOnly']) {
                        $subscriber['email'] = $entry[$field['id']];
                    }
                } elseif (is_object($field)) {
                    if ($field->type === "email" && !$field->adminOnly) {
                        $subscriber['email'] = $entry[$field->id];
                    }
                }
            }

            // custom fields

            $custom_fields = get_option($this->_slug . "_custom_fields");

            if ($custom_fields) {
                foreach ($feed['meta'] as $metakey => $metaval) {
                    if (strpos($metakey, 'mappedFields_') === 0 && $metaval) {
                        $field_id = (int)str_replace('mappedFields_', '', $metakey);
                        foreach ($custom_fields as $custom_field) {
                            if ($custom_field->id === $field_id) {
                                $subscriber['custom_fields'][$custom_field->name] = $entry[$metaval];
                                continue;
                            }
                        }
                    }
                }
            }

            if (empty($subscriber) || !array_key_exists('email', $subscriber)) {
                return;
            }

            $this->add_or_update_subscriber($subscriber, $listid);

        }

        protected function add_or_update_subscriber($subscriber, $listid) {
            $find_subscriber_response = wp_remote_get(
                $this->inboxfirst_url . "/mailing_lists/" . $listid . "/subscribers/" . urlencode($subscriber['email']),
                $this->http_args
            );

            if (!is_wp_error( $find_subscriber_response )) {

                $find_subscriber_body = json_decode($find_subscriber_response['body']);

                if ($find_subscriber_body->success) {
                    $subscriber_id = $find_subscriber_body->data[0]->id;
                    unset($subscriber['subscribe_time']);
                    $this->update_subscriber($subscriber, $listid, $subscriber_id);
                } else {
                    $this->create_subscriber($subscriber, $listid);
                }
            } else {
                echo $find_subscriber_response->get_error_message();
            }
        }

        protected function update_subscriber($subscriber, $listid, $subscriber_id) {
            $args = $this->http_args;
            $args['body'] = json_encode(array('subscriber' => $subscriber));
            $args['method'] = 'PUT';

            $subscribe_response = wp_remote_request(
                $this->inboxfirst_url . "/mailing_lists/" . $listid . "/subscribers/" . $subscriber_id,
                $args
            );

            if ( is_wp_error( $subscribe_response )) {
                echo $subscribe_response->get_error_message();
            }
        }

        protected function create_subscriber($subscriber, $listid) {
            $args = $this->http_args;
            $args['body'] = json_encode(array('subscriber' => $subscriber));

            $subscribe_response = wp_remote_post(
                $this->inboxfirst_url . "/mailing_lists/" . $listid . "/subscribers",
                $args
            );

            if ( is_wp_error( $subscribe_response )) {
                echo $subscribe_response->get_error_message();
            }
        }

        public function feed_list_columns() {
            return array(
                'feedName' => 'Name'
            );
        }
    }
}