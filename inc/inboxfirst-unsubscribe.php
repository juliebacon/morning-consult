<?php

if (class_exists("GFForms")) {
    GFForms::include_feed_addon_framework();

    class MCInboxfirstUnsubscribe extends GFFeedAddOn {

        protected $_version = "1.0";
        protected $_min_gravityforms_version = "1.7.9999";
        protected $_slug = "mc_inboxfirst_unsubscribe";
        protected $_path = "wp-content/themes/morning-consult/inc/inboxfirst-unsubscribe.php";
        protected $_full_path = __FILE__;
        protected $_title = "InboxFirst Unsubscribe Add-On";
        protected $_short_title = "InboxFirst Unsubscribe Add-On";
        protected $http_args;
        protected $inboxfirst_url = "http://if.inboxfirst.com/ga/api/v2/";
        protected $mc_inboxfirst_addon;
        protected $unsubscribes_to_do = array();
        protected $can_do_unsubs = true;

        public function __construct($mc_inboxfirst_addon) {
            parent::__construct();
            $this->mc_inboxfirst_addon = $mc_inboxfirst_addon;
        }

        public function init(){
            parent::init();
            $this->http_args = $this->mc_inboxfirst_addon->get_http_args();
        }

        public function feed_list_columns() {
            return array(
                'feedName' => 'Name'
            );
        }

        public function get_lists() {
            $lists = $this->mc_inboxfirst_addon->get_lists();

            // Not using for a dropdown, so remove the default option
            unset($lists[0]);

            // change this to work for the "field map" type
            foreach ($lists as $key => $value) {
                $lists[$key]['name'] = $value['value'];
                unset($lists[$key]['value']);
            }

            return $lists;
        }

        public function feed_settings_fields() {
            if (!$this->http_args) {
                return array();
            }

            $lists = $this->get_lists();

            return array(
                array(
                    "title"  => "InboxFirst Settings",
                    "fields" => array(
                        array(
                            "label"   => "Feed name",
                            "type"    => "text",
                            "name"    => "feedName",
                            "class"   => "small"
                        ),
                        array(
                            "name" => "mappedFields",
                            "label" => "List IDs",
                            "type" => "field_map",
                            "field_map" => $lists
                        ),
                        array(
                            "name" => "allListsField",
                            "label" => "Suppress From All Lists",
                            "type" => "field_map",
                            "field_map" => array(
                                array(
                                    'label' => 'All lists',
                                    'name' => 'all_lists'
                                )
                            )
                        ),
                        array(
                            "label"   => "Global Suppression List ID",
                            "type"    => "text",
                            "name"    => "suppressionListId",
                            "class"   => "small"
                        ),
                        array(
                            "name" => "condition",
                            "label" => "Condition",
                            "type" => "feed_condition",
                            "checkbox_label" => "Enable Condition",
                            "instructions" => "Process this feed if"
                        )
                    )
                )
            );
        }

        public function process_feed($feed, $entry, $form){

            foreach ($form['fields'] as $key => $value) {
                if ($value->type === "email") {
                    $email = $entry[$value->id];
                }
            }

            if (!$email) {
                return;
            }

            $do_suppress_field_id = $feed['meta']['allListsField_all_lists'];
            $suppress_list_id = (int)$feed['meta']['suppressionListId'];

            // should we add them to global suppression list?
            if ($suppress_list_id && $do_suppress_field_id && $entry[$do_suppress_field_id]) {
                $this->do_suppress($suppress_list_id, $email);
            } else {
                foreach ($feed['meta'] as $key => $value) {
                    // check to see if this is a list ID field & if it is set in the form entry
                    if (strpos($key, 'mappedFields_') === 0 && $value && $entry[$value]) {
                        $listid = (int)substr($key, strlen('mappedFields_'));
                        $this->do_unsubscribe($email, $listid);
                    }
                    unset($listid);
                }
            }
        }

        protected function do_unsubscribe($email, $listid) {

            if (!$listid) {
                return;
            }

            $subscriber = $this->find_subscriber($email, $listid);

            // processs if we found a subscriber and they were not already unsubscribed
            if ($subscriber && $subscriber->status !== "unsubscribed") {
                $args = $this->http_args;
                $args['body'] = json_encode(array(
                    'subscriber' => array(
                        'mailing_list_id' => $listid,
                        'status' => 'unsubscribed',
                        'email' => $email
                    )
                ));
                $args['method'] = 'PUT';

                $do_unsub_response = wp_remote_request(
                    $this->inboxfirst_url . "/mailing_lists/" . $listid . "/subscribers/" . $subscriber->id,
                    $args
                );

                // TODO: log result to gform meta?
                // if (!is_wp_error( $do_unsub_response )) {
                    // $do_unsub_body = json_decode($do_unsub_response['body']);
                    // if ($do_unsub_body->success) {
                    // }
                // }
            }
        }

        protected function do_suppress($listid, $email) {
            $args = $this->http_args;
            $args['body'] = json_encode(array(
                'data' => array($email)
            ));

            $do_suppress_response = wp_remote_post(
                $this->inboxfirst_url . "/suppression_lists/" . $listid . "/suppressed_addresses/create_multiple",
                $args
            );

            // TODO: log result to gform meta?
            // if (!is_wp_error( $do_suppress_response )) {
                // $do_suppress_body = json_decode($do_suppress_response['body']);
                // if ($do_suppress_body->success) {
                // }
            // }
        }

        protected function find_subscriber($email, $listid) {

            $find_subscriber_response = wp_remote_get(
                $this->inboxfirst_url . "/mailing_lists/" . $listid . "/subscribers/" . urlencode($email),
                $this->http_args
            );

            if (!is_wp_error( $find_subscriber_response )) {

                $find_subscriber_body = json_decode($find_subscriber_response['body']);

                if ($find_subscriber_body->success) {
                    return $find_subscriber_body->data[0];
                }
            }

            return false;
        }
    }
}