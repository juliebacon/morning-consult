<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

	<div class="page default 404">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<section class="error-404 not-found">
						<header class="page-header">
							<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'morning-consult' ); ?></h1>
						</header><!-- .page-header -->

						<div class="page-content">
							<p><?php _e( 'No posts. Try searching:', 'morning-consult' ); ?></p>

							<?php get_search_form(); ?>

						</div><!-- .page-content -->
					</section><!-- .error-404 -->
				</div>
			</div>
		</div>
	</div><!-- #primary -->

<?php get_footer(); ?>
