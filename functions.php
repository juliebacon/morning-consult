<?php
/**
 * Morning Consult 2015 functions and definitions
 *
 * @package Morning Consult 2015
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 940; /* pixels */
}

if ( ! function_exists( 'morning_consult_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function morning_consult_setup() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => 'Primary Menu',
		'secondary' => 'Secondary Menu',
		'footer' => 'Footer Menu',
		'footer-lower' => 'Footer Lower Menu',
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );
}
endif; // morning_consult_setup
add_action( 'after_setup_theme', 'morning_consult_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function morning_consult_widgets_init() {
	register_sidebar( array(
		'name'          => 'Small Ad Area',
		'id'            => 'small-ad-area',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '',
		'after_title'   => '',
	) );

	register_sidebar( array(
		'name'          => 'Wide Ad Area',
		'id'            => 'wide-ad-area',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '',
		'after_title'   => '',
	) );

	register_sidebar( array(
		'name'          => 'Wide Ad Area 2',
		'id'            => 'wide-ad-area-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '',
		'after_title'   => '',
	) );

	register_sidebar( array(
		'name'          => 'Subscribe Bar',
		'id'            => 'subscribe-bar',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '',
		'after_title'   => '',
	) );

	register_sidebar( array(
		'name'          => 'Footer',
		'id'            => 'footer-main',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="footer-widget row %2$s"><div class="col-xs-12"><div class="footer-widget-inner">',
		'after_widget'  => '</div></div></div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'morning_consult_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function morning_consult_scripts() {

	$url = get_site_url();
	$version = "20150617";

	if (strpos($url, '.dev') !== false || strpos($url, '.staging') !== false) {
		wp_enqueue_style( 'morning-consult-style', get_template_directory_uri() . "/dev/style.css", array(), $version );
	} else {
		wp_enqueue_style( 'morning-consult-style', get_stylesheet_uri(), array(), $version );
	}

	wp_enqueue_style( 'font-awesome', "//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" );

	wp_enqueue_script( 'jquery' );

	if (strpos($url, '.dev') !== false || strpos($url, '.staging') !== false) {
		wp_enqueue_script( 'morning-consult-all-js', get_template_directory_uri() . '/js/build/scripts.js', array('jquery'), $version, true );
	} else {
		wp_enqueue_script( 'morning-consult-all-js', get_template_directory_uri() . '/js/build/scripts.min.js', array('jquery'), $version, true );
	}

	// wp_enqueue_script( 'morning-consult-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	// 	wp_enqueue_script( 'comment-reply' );
	// }
}
add_action( 'wp_enqueue_scripts', 'morning_consult_scripts' );

function morning_consult_admin_scripts() {
	wp_enqueue_script( 'morning-brief-send', get_template_directory_uri() . '/js/morning-brief-send.js', array('jquery'), '20150706', true );
}
add_action( 'admin_enqueue_scripts', 'morning_consult_admin_scripts' );

/**
 * Bootstrap nav walker
 */
require get_template_directory() . '/inc/wp-bootstrap-navwalker.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom post types
 */
require get_template_directory() . '/inc/post-types.php';

/**
 * ACF post save hook
 */
require get_template_directory() . '/inc/acf-single-content.php';

/**
 * Gravity Forms Contactology Add-On
 */
require get_template_directory() . '/inc/class.Contactology.php';
require get_template_directory() . '/inc/contactology-gravity-forms.php';
require get_template_directory() . '/inc/contactology-unsubscribe.php';
$mc_contactology = new MorningConsultContactologyGravityForms();
$mc_contactology_unsubscribe = new MCUnsubscribe($mc_contactology);

require get_template_directory() . '/inc/brief-contactology.php';

/**
 * Gravity Forms InboxFirst Add-On
 */
require get_template_directory() . '/inc/inboxfirst-gravity-forms.php';
require get_template_directory() . '/inc/inboxfirst-unsubscribe.php';
$mc_inboxfirst = new MorningConsultInboxFirstGravityForms();
$mc_inboxfirst_unsubscribe = new MCInboxfirstUnsubscribe($mc_inboxfirst);