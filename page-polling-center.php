<?php
/**
 * Template name: Polling Center
 *
 * The template for displaying the polling center.
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

    <?php $exclude_from_recent_polls = array(); ?>

    <?php include(locate_template('inc/partials/polling/featured.php')); ?>

    <?php include(locate_template('inc/wide-ad-area.php')); ?>

    <?php include(locate_template('inc/partials/polling/recent.php')); ?>

    <?php include(locate_template('inc/wide-ad-area-2.php')); ?>

    <?php if(is_active_sidebar('subscribe-bar')) : ?>
        <div class="subscribe <?php echo $section_cat->slug; ?> subscribe-bar-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?php dynamic_sidebar( 'subscribe-bar' ); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

<?php get_footer(); ?>
