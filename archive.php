<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

	<div class="page archive">
		<div class="container">

			<?php if ( have_posts() ) : ?>

				<header class="page-header">
					<div class="row">
						<div class="col-xs-12 col-sm-8 col-sm-offset-2">
							<?php
								the_archive_title( '<h1 class="page-title">', '</h1>' );
								the_archive_description( '<div class="taxonomy-description">', '</div>' );
							?>
						</div>
					</div>
				</header><!-- .page-header -->

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<div class="row">
						<div class="col-xs-12 col-sm-8 col-sm-offset-2">
							<?php get_template_part( 'content', 'archive' ); ?>
						</div>
					</div>

				<?php endwhile; ?>

				<div class="row">
					<div class="col-xs-12 col-sm-8 col-sm-offset-2">
						<?php mc_the_posts_navigation(); ?>
					</div>
				</div>

			<?php else : ?>

				<?php get_template_part( 'content', 'none' ); ?>

			<?php endif; ?>
		</div>
	</div>

<?php get_footer(); ?>
