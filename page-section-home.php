<?php
/**
 * Template name: Section Home
 *
 * The template for displaying a section home page (health, energy, finance, tech, congress)
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

    <?php $section_cat = get_field('section_home_category'); ?>

    <?php include(locate_template( 'inc/partials/section/featured.php' )); ?>

    <?php get_template_part( 'inc/partials/section/brief-polling' ); ?>

    <?php include(locate_template('inc/wide-ad-area.php')); ?>

    <?php include(locate_template( 'inc/partials/section/stories.php' )); ?>

    <?php if ($section_cat->slug !== "congress") : ?>

        <?php get_template_part( 'inc/partials/section/opinions' ); ?>

    <?php endif; ?>

    <?php include(locate_template('inc/wide-ad-area-2.php')); ?>

    <?php if(is_active_sidebar('subscribe-bar')) : ?>
        <div class="subscribe <?php echo $section_cat->slug; ?> subscribe-bar-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?php dynamic_sidebar( 'subscribe-bar' ); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

<?php get_footer(); ?>
