<?php
/**
 * The template for displaying author archives
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

<?php
    global $authordata;
    if (get_class($authordata) === "WP_User") {
        include "inc/partials/author/author_user.php";
    } else if (get_class($authordata) === "stdClass") {
        include "inc/partials/author/author_guest.php";
    }
?>

    <?php include(locate_template('inc/wide-ad-area.php')); ?>

    <div class="page archive author">

        <div class="container">

            <?php if ( have_posts() ) : ?>

                <header class="page-header">
                    <h1 class="page-title">Posts by <?php echo get_the_author_meta( 'first_name' ); ?> <?php echo get_the_author_meta( 'last_name' ); ?></h1>
                </header><!-- .page-header -->

                <?php /* Start the Loop */ ?>
                <?php while ( have_posts() ) : the_post(); ?>

                    <div class="row">
                        <div class="col-xs-12">
                            <?php get_template_part( 'content', 'archive' ); ?>
                        </div>
                    </div>

                <?php endwhile; ?>

                <?php mc_the_posts_navigation(); ?>

            <?php else : ?>

                <?php get_template_part( 'content', 'none' ); ?>

            <?php endif; ?>
        </div>
    </div>

<?php get_footer(); ?>
