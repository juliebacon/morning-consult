<?php
/**
 * Template name: About
 *
 * @package Morning Consult 2015
 */

get_header(); ?>

    <?php while ( have_posts() ) : the_post(); ?>

        <section class="header" data-bg="<?php the_field('about_header_background_image'); ?>">
            <div class="inner">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <?php the_field('about_header_content'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="intro">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <?php the_field('about_intro_content'); ?>
                        <a class="btn btn-default btn-lg" href="/subscribe"><?php the_field('about_intro_subscribe_link_text'); ?></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="polling">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <?php the_field('about_polling'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p><?php the_field('about_most_recent_polls_text'); ?>
                    </div>
                </div>
                <div class="row">
                    <?php get_template_part( 'inc/partials/about/recent-polls' ); ?>
                </div>
            </div>
        </section>

        <section class="mid" data-bg="<?php the_field('about_mid_section_background_image'); ?>">
            <div class="inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php the_field('about_mid_section_content'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <?php $readers_images = get_field('about_readers_images'); ?>
        <section class="readers count-<?php echo count($readers_images); ?>">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">

                        <div class="content">
                            <?php the_field('about_readers_content'); ?>
                        </div>

                        <div class="row">
                            <?php
                            if( have_rows('about_readers_images') ):

                                $about_people_counter = 0;
                                while ( have_rows('about_readers_images') ) : the_row();
                                    if (count($readers_images) === 4 || count($readers_images) === 2) {
                                        echo '<div class="reader col-xs-12 col-sm-6">';
                                    } elseif (count($readers_images) === 3) {
                                        echo '<div class="reader col-xs-12 col-sm-4">';
                                    } elseif (count($readers_images) === 1) {
                                        echo '<div class="reader col-xs-12 col-sm-8 col-sm-offset-2">';
                                    }

                                    if (get_sub_field('link')) :
                                        echo '<a href="' . get_sub_field('link') . '">';
                                    endif;
                                    echo '<img src="' . get_sub_field('image') . '">';
                                    if (get_sub_field('link')) :
                                        echo '</a>';
                                    endif;
                                    echo "</div>";
                                    $about_people_counter++;
                                endwhile;
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="advertise" id="advertise">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <?php the_field('about_advertise_content'); ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="who">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <?php the_field('about_who_content'); ?>
                    </div>
                </div>
                <div class="row">
                    <?php get_template_part( 'inc/partials/about/people' ); ?>
                </div>
            </div>
        </section>

    <?php endwhile; ?>

<?php get_footer(); ?>
