<?php
/**
 * Template name: Home
 *
 * The home page template.
 *
 * @package Morning Consult 2015
 */
?>

<?php get_header(); ?>

    <?php get_template_part( 'inc/partials/home/stories' ); ?>

    <?php get_template_part( 'inc/partials/home/polling' ); ?>

    <?php include(locate_template('inc/wide-ad-area.php')); ?>

    <?php get_template_part( 'inc/partials/home/opinions' ); ?>

    <?php if(is_active_sidebar('subscribe-bar')) : ?>
        <div class="subscribe subscribe-bar-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?php dynamic_sidebar( 'subscribe-bar' ); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

<?php get_footer(); ?>
