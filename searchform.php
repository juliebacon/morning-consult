<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <div class="row">
        <div class="col-xs-12">
            <div class="input-group input-group-lg">
                <input type="text" class="form-control search-input" placeholder="Search for..." name="s" value="<?php echo get_search_query(); ?>">
                <span class="input-group-btn">
                    <button class="btn btn-default search-button" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div><!-- /input-group -->
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</form>