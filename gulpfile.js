var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var compass = require( 'gulp-compass' );
var livereload = require('gulp-livereload');

var paths = {
    "scripts": [
        "js/bootstrap.js",
        "js/jquery.backstretch.min.js",
        "js/velocity.js",
        "js/velocity.ui.js",
        "js/main.js"
    ]
};

// Watch Files For Changes
gulp.task('watch', function() {
    livereload.listen();

    gulp.watch(
        paths['scripts'],
        ['scripts-dev']
    );

    gulp.watch(
        ['sass/*.scss','sass/**/*.scss','sass/**/**/*.scss'],
        ['sass-dev']
    );
});

gulp.task('scripts-dev', function() {
    return gulp.src(paths['scripts'])
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('js/build'));
});

gulp.task('scripts', function() {
    return gulp.src(paths.scripts)
        .pipe(uglify())
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest('js/build'));
});

gulp.task( 'sass-dev', function(){
    console.log('Compiling SASS (dev)');
    gulp.src('sass/style.scss')
        .pipe(compass({
          config_file: 'config.dev.rb',
          css: 'dev',
          sass: 'sass',
          style: 'expanded',
          sourcemap: false
    }))
    .pipe(gulp.dest('dev'))
    .pipe(livereload());
});

gulp.task( 'sass', function(){
    console.log('Compiling SASS (production)');
    gulp.src('sass/style.scss')
        .pipe(compass({
          config_file: 'config.rb',
          css: '',
          sass: 'sass'
    }));
});

gulp.task('default', ['sass-dev','sass', 'scripts', 'scripts-dev']);